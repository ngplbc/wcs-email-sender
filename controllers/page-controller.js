const showHomePage = (req, res, next) => {
  return res.render("index", { message: req.message });
};

module.exports = { showHomePage };