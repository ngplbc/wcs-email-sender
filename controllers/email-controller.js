const nodemailer = require("nodemailer");

const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "5ee8c114bd4134",
    pass: "87fde57fb837a5"
  }
});

const sendEmail = (req, res, next) => {
  const { subject, mail } = req.body;
  
  transport.sendMail({
    from: "demo-aa9ab3@inbox.mailtrap.io", // Sender
    to: "supergrandma@yopmail.com", // Recipients
    subject: subject, // Subject
    text: mail, // Text body
    html: `<b>${mail}</b>` // HTML body
  }, (err, response) => {
    if(err) console.log(err);
    else {
      req.message = "Message successfully sent to " + response.accepted[0];
      next();
    }
  });
};

module.exports = { sendEmail }