const express = require("express");
const router = express.Router();
const { showHomePage } = require("../controllers/page-controller");
const { sendEmail } = require("../controllers/email-controller");

router.get("/", showHomePage);

router.post("/askForCookiesRecipe", sendEmail, showHomePage);

module.exports = router;